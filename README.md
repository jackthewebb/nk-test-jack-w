# NK test Jack W

The test submission is in the Unity project (Blob detection scene) and can be viewed best in the Scene window.
After pressing the play button, the script will run, 
marking the Center point of each island(red pixel) and the most central island (green pixel).
The results for the larger images will be a bit difficult to view, so zooming in with the unity scene camera is required.

Multiple image conversions are displayed in the project. This is to show that the program will perform under the following unexpected use cases:

-The amount of islands is greater or less than 8 (including 0)
-The image size is larger/smaller than 100x100 pixels
-the image size is uneven (e.g. 400x200 pixels)
-An island is made up of only one pixel             
-There are colors in the image that aren't black or white
-The center point of the image is not a black pixel 
-Only black pixels are on the image
-an Island has a very large amount of pixels (e.g. over 2 million)

#Summary of the program

First, using the Texture2D.GetPixel() method, The program loops through each pixel of the image (which is stored as a Texture2D).
Using a 2D array of integers, I store a 1 for a black pixel, while leaving the int as 0 for all other pixels.
each black pixel's x/y coordinates are added to a list of Vector2Ints.
If there are no black pixels, the program will not progress any further and will print a console message.

I use the type Vector2Int because it can simply and efficiently store the X and Y of each pixel.

The program then loops through every black pixel in this list, running a DFS algorithm on them.
This will do the following
-Mark them as visited, so that they will not be checked by the DFS algorithm again
-Check whether this pixel has the largest/smallest X and Y values of its island (for calculating the center point)
-Run the DFS algorithm on each adjacent black pixel.

I chose to use a DFS(depth first search) algorithm, due to its ability to check every adjacent pixel without having to check a pixel multiple times.
However, this caused a stack overflow when trying to loop through an island that was around 7000 pixels and over.
I then switched to an iterative variation of the DFS, which pushes each black pixel to a stack before performing another DFS, which solved this issue.

After looping through each pixel on the island, the stored max and min Y values are used to find the center point of the island.
The X and Y coordinates for the center point of each island are added to a list of Vector2Ints, and are also marked as a 2 on the 2D array. 

If this center point is on a white pixel, then the program will loop through the island's pixels, calculate the distance from each pixel to the center point, and choose the pixel closest to the center point to be the center. This was implemented for cases such as donut or crescent-shaped
islands where the center point is not on the island. I also tried switching to finding the most central pixel of each island(the shortest distance from all the other pixels) to fix this issue, however, this required a lot more calculations to do, so it wasn't worth it.

This repeats until all of the black pixels have been visited.

Now that there is a list of all island center points, the program loops through this list and for each island center:

-Uses Pythagoras to calculate the distance between every other island center.
-adds these to get the sum of all distances

The island center with the lowest total distance is the most central island, and its coordinates are marked as a 3 in the 2d array of pixels.

The program then loops through every pixel in the 2D pixel array and creates a new texture to apply to a sprite. on this new texture, the color of each pixel is based on the value of the 2D pixel array (0 = white, 1 = black, 2 = red, 3 = green). 