using System.Collections.Generic;
using UnityEngine;

public class BlobDetector : MonoBehaviour
{

    private int gridHeight, gridWidth;

    private bool[,] visited;
    private int[,] pixelArray;

    private List<Vector2Int> blackPixels = new List<Vector2Int>();
    private List<Vector2Int> island = new List<Vector2Int>();

    //directional arrays for the dfs method
    private readonly int[] dx = { -1, 1, 0, 0 };
    private readonly int[] dy = { 0, 0, -1, 1 };

    //use this field to add new images(make sure the texture type is set to Sprite(2D and UI)
    //and Read/Write is enabled in the "Advanced" tab
    [SerializeField] 
    private Texture2D texture;

    void Start()
    {
        FindCenterPoints();
    }

    private void FindCenterPoints()
    {

        // Load the image as a Texture2D, then set the grid dimensions using the size of the image
        if (texture == null) texture = Resources.Load<Texture2D>("islands");

        gridHeight = texture.height;
        gridWidth = texture.width;

        pixelArray = new int[gridWidth, gridHeight];
        visited = new bool[gridWidth, gridHeight];

        //Loop through each pixel in the image
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                Color pixelColor = texture.GetPixel(x, y);

                //fill 2D pixelArray with 1 for black pixels, add to the list of black pixels
                if (pixelColor == Color.black)
                {
                    blackPixels.Add(new Vector2Int(x, y));
                    pixelArray[x, y] = 1;
                }
            }
        }

        //check to see if any black pixels were found to halt the program in the case that there are none found
        if (blackPixels.Count == 0)
        {
            Debug.Log("Zero black pixels were detected on image: " + gameObject.name);
            return;
        }

        //Loop through all black pixels and run the dfs method on them if they haven't been checked yet

        List<Vector2Int> islandCenters = new List<Vector2Int>();
        int maxX, minX, maxY, minY;

        foreach (Vector2Int i in blackPixels)
        {
            //if new island discovered, find the center point of this island and add to list
            if (!visited[i.x, i.y])
            {
                maxX = int.MinValue;
                minX = int.MaxValue;
                maxY = int.MinValue;
                minY = int.MaxValue;

                Vector2Int centerPoint;

                island = new List<Vector2Int>();

                Stack<Vector2Int> stack = new Stack<Vector2Int>();
                stack.Push(i);

                //perform an Iterative DFS(Depth First Search) on each pixel that is added to the stack

                Vector2Int s;
                while (stack.Count > 0)
                {
                    s = stack.Peek();
                    stack.Pop();
                    visited[s.x, s.y] = true;

                    island.Add(new Vector2Int(s.x, s.y));

                    //check if the pixel's coordinates are the max/min

                    if (s.x > maxX) maxX = s.x;
                    if (s.y > maxY) maxY = s.y;
                    if (s.x < minX) minX = s.x;
                    if (s.y < minY) minY = s.y;
                    
                    //push each valid adjacent pixel to the stack
                    for (int k = 0; k < 4; k++)
                    {
                        int next_x = s.x + dx[k];
                        int next_y = s.y + dy[k];
                        if (PointValid(next_x, next_y))
                            stack.Push(new Vector2Int(next_x, next_y));
                    }   
                }

                int middleX = (int)Mathf.Round((maxX + minX) / 2);
                int middleY = (int)Mathf.Round((maxY + minY) / 2);

                //move center point to the nearest pixel on the island if not already on the island
                if (pixelArray[middleX, middleY] == 0)
                {
                    int closestPixel = int.MaxValue;
                    float lowestTotalDistance = float.MaxValue;

                    //calculate distances of each pixel on the island to find which one is closest to the center point
                    for (int k = 0; k < island.Count; k++)
                    {
                        float A = middleX - island[k].x;
                        float B = middleY - island[k].y;
                        float distance = Mathf.Sqrt((A * A) + (B * B));

                        if (distance < lowestTotalDistance)
                        {
                            lowestTotalDistance = distance;
                            closestPixel = k;
                        }
                    }
                    centerPoint = island[closestPixel];
                }
                else
                {
                    centerPoint = (new Vector2Int(middleX, middleY));
                }

                islandCenters.Add(centerPoint);
                pixelArray[centerPoint.x, centerPoint.y] = 2;
            }
        }

        int mostCentralpixel = int.MaxValue;
        float lowestDistance = float.MaxValue;

        //loop through all of the island's center points
        for (int i = 0; i < islandCenters.Count; i++)
        {
            float totalDistance = 0;

            //loop through the islands to compare center point of island i to each other one
            for (int j = 0; j < islandCenters.Count; j++)
            {
                //find the distance between the two pixels
                if (i != j)
                {
                    float A = islandCenters[i].x - islandCenters[j].x;
                    float B = islandCenters[i].y - islandCenters[j].y;
                    //C = SQRT(A^2 +B^2)
                    float distance = Mathf.Sqrt((A * A) + (B * B));

                    totalDistance += distance;
                }
            }
            //keep track of the lowest total distance
            if (totalDistance < lowestDistance)
            {
                lowestDistance = totalDistance;
                mostCentralpixel = i;
            }
        }

        //set the most central island's pixel array value to 3
        pixelArray[islandCenters[mostCentralpixel].x, islandCenters[mostCentralpixel].y] = 3;

        //create the new image at the end, because all of the calculations are finished
        DrawImage();
    }

    //check if the coordinates are within the grid boundary and the point hasnt been visited yet and the pixel is black
    private bool PointValid(int x, int y)
    {
        return (x >= 0 && x < gridWidth && y >= 0 && y < gridHeight && !visited[x, y] && pixelArray[x, y] == 1);
    }

    //create a new texture using the information from the pixel array  
    private void DrawImage()
    {

        Texture2D texture = new Texture2D(gridWidth, gridHeight, TextureFormat.ARGB32, false);
        Color[] colors = { Color.white, Color.black, Color.red, Color.green };

        //loop through the pixel array and select a color from the colors list depending on the int value of the pixel

        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                texture.SetPixel(x, y, colors[pixelArray[x, y]]);
            }
        }

        //draw a new sprite and apply it to the sprite renderer attached to the gameobject 
        texture.Apply();
        GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, gridWidth, gridHeight), new Vector2(0.5f, 0.5f));
    }
}


